package ro.dataProvider;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import java.util.concurrent.TimeUnit;

public final class LoginFactory {

    private WebDriver driver = new ChromeDriver();
    private static LoginFactory inst;

    private LoginFactory() {

    }

    public static LoginFactory getInstance() {
        if (inst == null) {
            inst = new LoginFactory();
        }
        return inst;
    }


    public boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (org.openqa.selenium.NoSuchElementException e) {
            return false;
        }
    }

    public void openAndMaximizeWindow(ConfigFileReader configFileReader) {

        System.setProperty("webdriver.chrome.driver", configFileReader.getDriverPath());

        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(configFileReader.getImplicitlyWait(), TimeUnit.SECONDS);

        driver.navigate().to(configFileReader.getApplicationUrl());
        System.out.println("URL is entered.");
        System.out.println("The website is maximized and opened.");
    }


    public void setValidUsername(String username, By usernameBy) {
        WebElement email = driver.findElement(usernameBy);
        email.sendKeys(username);
    }

    public void setValidPassword(String password, By passwordBy) {
        WebElement passwd = driver.findElement(passwordBy);
        passwd.sendKeys(password);
    }

    public void setInvalidUsername(String username, By usernameBy) {
        WebElement email = driver.findElement(usernameBy);
        email.sendKeys(username);
    }

    public void setInvalidPassword(String password, By passwordBy) {
        WebElement passwd = driver.findElement(passwordBy);
        passwd.sendKeys(password);
    }

    public void clickSubmit(By clickBy) {
        WebElement clickSubmit = driver.findElement(clickBy);
        clickSubmit.click();
    }

    public void checkLogo(By logoBy) {
        WebElement image = driver.findElement(logoBy);

        if (image.isDisplayed() == true) {
            System.out.println("Logo image is present on page.");
        } else {
            System.out.println("Logo image is not present on page.");
        }
    }

    public void clickLogOut(By logOutBy) {
        WebElement logOut = driver.findElement(logOutBy);
        logOut.click();
    }

    public WebDriver getDriver() {
        return driver;
    }




}

package pages;


import org.openqa.selenium.WebDriver;
import ro.dataProvider.ConfigFileReader;

public class HomePage {
	WebDriver driver;
	ConfigFileReader configFileReader;

	public HomePage() {

	}

	public HomePage(WebDriver driver, ConfigFileReader configFileReader) {
		this.driver = driver;
		this.configFileReader = configFileReader;
	}

}

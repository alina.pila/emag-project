package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.util.List;

public class MyWishlists {

    protected void selectDeleteRow(WebElement wishlistTable, int whichRow) {
        List<WebElement> deleteButtons = wishlistTable.findElements ( By.className ( "wishlist_delete" ) );
        WebElement tr = deleteButtons.get ( whichRow );
        tr.findElement ( By.className ( "icon" ) ).click ();
    }

    protected void selectViewRow(WebElement viewWishlistTable, int viewWhichRow) {
        List<WebElement> viewButtons = viewWishlistTable.findElements ( By.linkText ( "View" ) );
        WebElement a = viewButtons.get ( viewWhichRow );
        a.click ();
    }

}

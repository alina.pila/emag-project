package tests;

import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import ro.dataProvider.DefaultLogin;

public class MyPersonalInfo_Tests {

    private By myPersonalInfoElement = By.xpath("//div[@id='center_column']/div/div[1]/ul[@class='myaccount-link-list']//a[@title='Information']/span[.='My personal information']");
    private By radioSocialTitleElement = By.id("id_gender2");
    private By firstNameElement = By.id("firstname");
    private By lastNameElement = By.id("lastname");
    private By emailPersonalInfoElement = By.xpath("/html//input[@id='email']");
    private By currentPasswordElement = By.cssSelector("#old_passwd");
    private By newPasswordElement = By.cssSelector("#passwd");
    private By confirmNewPasswordElement = By.cssSelector("#confirmation");
    private By savePersonalInfoElement = By.cssSelector("fieldset .form-group:nth-of-type(11) span");

    public MyPersonalInfo_Tests() { }

    @Test
    public void validUpdatePersonalInfo(){
        DefaultLogin instance = new DefaultLogin();
        instance.login();
        WebDriver driver = instance.getDriver();

        WebElement myPersonalInfo = driver.findElement(myPersonalInfoElement);
        myPersonalInfo.click();
        System.out.println("My personal information is opened.");

        WebElement radioSocialTitle = driver.findElement(radioSocialTitleElement);
        radioSocialTitle.click();

        WebElement firstName = driver.findElement(firstNameElement);
        firstName.clear();
        firstName.sendKeys("Popescu");

        WebElement lastName = driver.findElement(lastNameElement);
        lastName.clear();
        lastName.sendKeys("Irina");

        WebElement emailPersonalInfo = driver.findElement(emailPersonalInfoElement);
        emailPersonalInfo.clear();
        emailPersonalInfo.sendKeys("irina.popescu@gmail.com");

        WebElement currentPassword = driver.findElement(currentPasswordElement);
        currentPassword.clear();
        currentPassword.sendKeys("oldpassword");

        WebElement newPassword = driver.findElement(newPasswordElement);
        newPassword.sendKeys("newpassword");

        WebElement confirmNewPassword = driver.findElement(confirmNewPasswordElement);
        confirmNewPassword.sendKeys("newpassword");

        WebElement savePersonalInfo = driver.findElement(savePersonalInfoElement);
        savePersonalInfo.click();
        System.out.println("Personal information is saved.");
    }

}
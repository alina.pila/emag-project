package tests;

import org.junit.Assert;
import ro.dataProvider.DefaultLogin;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Search_Tests {

    private By searchTextElement = By.id("search_query_top");
    private By searchButtonElement = By.cssSelector(".button-search");
    private By searchSpecificTextElement = By.cssSelector(".ac_results ul li:nth-of-type(3)");
    private By notificationMessageElement = By.cssSelector(".alert-warning");
    public Search_Tests() {

    }

    @Test
    public void performValidSearch() {
        DefaultLogin instance = new DefaultLogin();
        instance.login();
        WebDriver driver = instance.getDriver();

        WebElement searchText = driver.findElement(searchTextElement);
        searchText.sendKeys("dress");
        System.out.println("Search criterion is entered.");

        WebElement selectSearchText = driver.findElement(searchSpecificTextElement);
        selectSearchText.click();
        System.out.println("Select specific text element to search upon.");

        WebElement searchButton = driver.findElement(searchButtonElement);

        searchButton.click();
        System.out.println("Search button is clicked.");

    }

    @Test
    public void performInvalidSearch() {
        DefaultLogin instance = new DefaultLogin();
        instance.login();
        WebDriver driver = instance.getDriver();

        WebElement searchTextInvalid = driver.findElement(searchTextElement);
        searchTextInvalid.sendKeys("");
        System.out.println("Search by null value.");

        WebElement searchButtonInvalid = driver.findElement(searchButtonElement);
        searchButtonInvalid.click();
        System.out.println("Search button is clicked.");

        String expectedNotificationMessage = "Please enter a search keyword";
        String notificationInstance = instance.getDriver().findElement(notificationMessageElement).getText();
        Assert.assertTrue("Please enter a search keyword", notificationInstance.contains(expectedNotificationMessage));
        System.out.println("Notification message is present on page.");

        driver.quit();
    }
}
